Some System Admin tools and personal code.

Filename | Description
------------ | -------------
count_characters.py | a simle program that counts characters in a string 
guided_mediation.py | My attempt at replacing a Meditation guide with a shell script. I have trouble spelling it.
memory_calc.py | a program to calculate memory use from ps tree
time_wizard.py | used to calculate times in the past or future in a different timezone from the origin.
block_meP3.py  | a python3 formated automated simple firewall to prevent simple DOS attacks 
block_me.py  | a python2 formated automated simple firewall to prevent simple DOS attacks 
